var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: {
        default: './range-facet-demo.js'
    },
    devtool: 'source-map',
    output: {
        publicPath: '/',
        path: path.join(__dirname, 'public'),
        filename: '[hash].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules\/(?!(([^\/]+?\/){1,2}(src|es6)))/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ["es2015", { "modules": false }],
                            ["react"],
                            ["stage-0"]
                        ]
                    }
                }],
            },
            {
                test: /\.css/,
                use: ExtractTextPlugin.extract({
                    "fallback": "style-loader",
                    use: ["css-loader"]
                })
            },
            {
                test: /\.scss/,
                use: ExtractTextPlugin.extract({
                    "fallback": "style-loader",
                    use: ["css-loader", "sass-loader"]
                })
            }
        ]
    },
    plugins: [
		new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"development"'
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin({
            filename: '[contenthash].css',
            allChunks: true,
        }),
        new HtmlWebpackPlugin({
            template: './index.html'
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        noInfo: true,
        hot: true,
        inline: true,
        port: 8080
    }
};