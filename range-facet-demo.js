import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Range } from 'rc-slider';


import './range-slider.scss';
import './node_modules/rc-slider/dist/rc-slider.css';


const RANGE_FACET = {
    "id": "pengar",
    "selectionType": "SINGLE",
    "displayName": "Pengar",
    "filters": [
        {
            "displayName": "0.0",
            "query": "q=*&pengar.min=0.0&pengar.max=99.99",
            "count": 10,
            "selected": false,
        },
        {
            "displayName": "100.0",
            "query": "q=*&pengar.min=100.0&pengar.max=199.99",
            "count": 35,
            "selected": false,
        },
        {
            "displayName": "200.0",
            "query": "q=*&pengar.min=200.0&pengar.max=299.99",
            "count": 24,
            "selected": false,
        },
        {
            "displayName": "300.0",
            "query": "q=*&pengar.min=300.0&pengar.max=399.99",
            "count": 39,
            "selected": false,
        },
        {
            "displayName": "400.0",
            "query": "q=*&pengar.min=400.0&pengar.max=499.99",
            "count": 21,
            "selected": false,
        },
        {
            "displayName": "500.0",
            "query": "q=*&pengar.min=500.0&pengar.max=599.99",
            "count": 81,
            "selected": false,
        },
        {
            "displayName": "600.0",
            "query": "q=*&pengar.min=600.0&pengar.max=699.99",
            "count": 65,
            "selected": false,
        },
        {
            "displayName": "700.0",
            "query": "q=*&pengar.min=700.0&pengar.max=799.99",
            "count": 74,
            "selected": false,
        },
        {
            "displayName": "800.0",
            "query": "q=*&pengar.min=800.0&pengar.max=899.99",
            "count": 32,
            "selected": false,
        },
        {
            "displayName": "900.0",
            "query": "q=*&pengar.min=900.0&pengar.max=999.99",
            "count": 23,
            "selected": false,
        },
        {
            "displayName": "1000.0",
            "query": "q=*&pengar.min=1000.0",
            "count": 8,
            "selected": false,
        },
    ]
};


const VALID_KEY_CODES = [8, 9, 37, 39, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105];
class RangeSlider extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = { range: [props.range.min, props.range.max] };
    }

    onCheckKey = e => {
        if (VALID_KEY_CODES.indexOf(e.keyCode) >= 0) {
            return true;
        } else if (e.keyCode === 13) {
            e.stopPropagation();
            e.preventDefault();

            this.onChangeRangeInput(e);

            return false;
        } else {
            e.stopPropagation();
            e.preventDefault();

            return false;
        }
    };

    onChangeRangeInput = e => {
        let min = parseFloat(this.refs.min.innerHTML.replace(' ', '')),
            max = parseFloat(this.refs.max.innerHTML.replace(' ', ''));

        let state = Object.assign({}, this.state);

        if (min !== this.props.range.min || max !== this.props.range.max) {
            state.range = [min, max];
            this.onFilterRange([min, max]);
        }
        this.setState(state);
    };

    onUpdateRange = range => {
        this.setState({ range });
    };
    onFilterRange = range => this.props.onRangeChange(range[0], range[1]);

    render() {
        let total = this.props.items.reduce((acc, current) => acc + current.count, 0),
            bars = {},
            highest = 0,
            percentage = value => (100 / total) * value;

        this.props.items.forEach(item => {
            let percent = percentage(item.count);
            bars[item.value] = percent;
            highest = highest > percent ? highest : percent;
        });

        let ratio = highest > 100 ? 1 : Math.floor(100 / highest);

        return (
            <div data-am-rangeslider="">
                <div className="range-input-container">
                    <span className="range-input">
                        <div ref="min" className="input" contentEditable="true" onKeyDown={this.onCheckKey} onBlur={this.onChangeRangeInput}>{this.state.range[0]}</div>
                    </span>
                    <span className="m-x-mini">-</span>
                    <span className="range-input">
                        <div ref="max" className="input" contentEditable="true" onKeyDown={this.onCheckKey} onBlur={this.onChangeRangeInput}>{this.state.range[1]}</div>
                        kr
                    </span>
                </div>
                <div className="range-slider">
                    <div className="histogram-container">
                        <div className="histogram-chart">
                            {this.props.items.map(item => {
                                let isActive = item.value >= this.state.range[0] && item.value <= this.state.range[1];

                                return (
                                    <div key={item.value} className="histogram-column">
                                        <div className={"column-fill" + (isActive ? " is-active" : "")} style={{ height: `${(ratio * bars[item.value])}%` }}></div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                    <Range min={this.props.min} max={this.props.max} value={this.state.range} onChange={this.onUpdateRange} onAfterChange={this.onFilterRange} />
                </div>
                <div className="range-span">
                    <div className="range-span-low">{this.props.min} kr</div>
                    <div className="range-span-high">{this.props.max} kr</div>
                </div>
            </div>
        )
    }
}
RangeSlider.propTypes = {
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.number.isRequired,
        count: PropTypes.number.isRequired,
    })).isRequired,
    range: PropTypes.shape({
        min: PropTypes.number.isRequired,
        max: PropTypes.number.isRequired,
    }).isRequired,
    onRangeChange: PropTypes.func.isRequired,
};


const onRangeChange = (min, max) => console.log(`Query ElasticSearch for documents with a value between ${min} and ${max}`);
const FacetSlider = props => {
    let min = Number.MAX_SAFE_INTEGER,
        max = Number.MIN_SAFE_INTEGER,
        items = RANGE_FACET.filters.map(({ displayName, count }) => {
            let value = parseFloat(displayName);

            min = value < min ? value : min;
            max = value > max ? value : max;

            return {
                value,
                count,
            };
        });

    return (
        <div style={{ margin: "auto", width: "600px" }}>
            <h1>Pengar</h1>
            <RangeSlider items={items} range={{ min, max }} min={min} max={max} onRangeChange={onRangeChange} />
        </div>
    );
};

ReactDOM.render(<FacetSlider />, document.getElementById('range-slider'));